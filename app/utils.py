def is_number(n):
    try:
        float(n.replace(',', '.'))
        return True
    except ValueError:
        return False
