import os
import errno
from flask import Flask


def create_app():
    # cria e configura o app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=os.environ.get('SECRET', 'dev'),
        DATABASE=os.path.join(app.instance_path, os.environ.get('DATABASE', 'db')),
    )

    # garante que a pasta de instancia exista
    try:
        os.makedirs(app.instance_path)
    except OSError as e:
        if e.errno != errno.EEXIST: raise
        pass

    # importa e registra os blueprints
    from . import db
    db.init_app(app)

    from . import mantimento
    app.register_blueprint(mantimento.bp)
    app.add_url_rule('/', endpoint='index')

    return app
