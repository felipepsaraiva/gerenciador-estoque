from flask import Blueprint, redirect, render_template, request, url_for, jsonify, abort
from app.db import get_db
from app.utils import is_number

bp = Blueprint('mantimento', __name__)


@bp.route('/')
def index():
    db = get_db()
    mantimentos = db.execute('SELECT * FROM mantimento').fetchall()
    return render_template('index.html', mantimentos=mantimentos)


@bp.route('/novo-mantimento', methods=('GET', 'POST'))
def novo():
    erros = {}

    if request.method == 'POST':
        nome = request.form['nome']
        marca = request.form['marca']
        descricao = request.form['descricao']
        consumo_diario = request.form['consumo-diario']
        quantidade_min = request.form['quantidade-min']

        erros = validar_mantimento(request.form)

        if len(erros.values()) < 1:
            db = get_db()
            db.execute(
                'INSERT INTO mantimento (nome, marca, descricao, consumo_diario, quantidade_min)'
                ' VALUES (?, ?, ?, ?, ?)',
                (nome, marca, descricao, consumo_diario, quantidade_min)
            )

            db.commit()
            return redirect(url_for('index'))

    return render_template('mantimento.html', erros=erros)


@bp.route('/mantimento/<int:id>', methods=('GET', 'POST'))
def editar(id):
    if request.method == 'GET':
        mantimento = get_mantimento(id)
        return render_template('mantimento.html', id=id, mantimento=mantimento)

    elif request.method == 'POST':
        nome = request.form['nome']
        marca = request.form['marca']
        descricao = request.form['descricao']
        consumo_diario = request.form['consumo-diario']
        quantidade_min = request.form['quantidade-min']

        erros = validar_mantimento(request.form)
        if len(erros.values()) > 0:
            return render_template('mantimento.html', id=id, erros=erros)

        if len(erros.values()) < 1:
            db = get_db()
            db.execute(
                'UPDATE mantimento'
                ' SET nome = ?, marca = ?, descricao = ?, consumo_diario = ?, quantidade_min = ?'
                ' WHERE id = ?',
                (nome, marca, descricao, consumo_diario, quantidade_min, id)
            )

            db.commit()
            return redirect(url_for('index'))


def get_mantimento(id):
    mantimento = get_db().execute(
        'SELECT id, nome, marca, descricao, consumo_diario, quantidade_min'
        ' FROM mantimento WHERE id = ?',
        (id,)
    ).fetchone()

    if mantimento is None:
        abort(404, "Não existe mantimento com id {0}.".format(id))

    return mantimento


def validar_mantimento(form):
    erros = {}
    nome = request.form['nome']
    consumo_diario = request.form['consumo-diario']
    quantidade_min = request.form['quantidade-min']

    if not nome:
        erros['nome'] = 'Nome é obrigatório!'

    if not consumo_diario:
        erros['consumo_diario'] = 'Consumo diário é obrigatório!'
    elif not is_number(consumo_diario):
        erros['consumo_diario'] = 'Consumo diário deve ser um número!'

    if not quantidade_min:
        erros['quantidade_min'] = 'Quantidade mínima é obrigatória!'
    elif not is_number(quantidade_min):
        erros['quantidade_min'] = 'Quantidade mínima deve ser um número!'

    return erros
