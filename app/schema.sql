DROP TABLE IF EXISTS mantimento;
DROP TABLE IF EXISTS transacao;

CREATE TABLE mantimento (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nome TEXT NOT NULL,
  marca TEXT,
  descricao TEXT,
  consumo_diario INTEGER NOT NULL DEFAULT 0,
  quantidade_min INTEGER NOT NULL DEFAULT 0,
  quantidade_atual INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE transacao (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  mantimento_id INTEGER NOT NULL,
  data TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  quantidade_antiga INTEGER NOT NULL,
  quantidade_nova INTEGER NOT NULL,
  FOREIGN KEY (mantimento_id) REFERENCES mantimento (id)
);
