# Histórias de usuário

1. Como usuário, eu quero cadastrar um mantimento no sistema para fazer o controle de estoque desse mantimento
2. Como usuário, eu quero visualizar a lista de mantimentos com suas informações para ter uma visão geral do que está cadastrado
3. Como usuário, eu quero alterar a quantidade mínima de cada mantimento
4. Como usuário, eu quero alterar o consumo diário de cada mantimento
5. Como usuário, eu quero adicionar e remover mantimentos do estoque
6. Como usuário, eu quero gerar uma lista de compra diária, semanal ou mensal

# Critérios de aceitação

### 1. Como usuário, eu quero cadastrar um mantimento no sistema para fazer o controle de estoque desse mantimento

- Deverá ter um botão para iniciar o cadastro
- Informar o nome, marca (opcional), descrição (opcional)
- Cadastrar a quantidade mínima
- Cadastrar o consumo diário
- Ao finalizar o cadastro, perguntar se o usuário deseja iniciar um novo

### 2. Como usuário, eu quero visualizar a lista de mantimentos com suas informações para ter uma visão geral do que está cadastrado

- Exibir quantidades absolutas e percentuais

### 3. Como usuário, eu quero alterar a quantidade mínima de cada mantimento

- Deverá haver um botão em cada mantimento da lista para iniciar a alteração
- O novo valor deve ser informado em uma caixa de texto
- O novo valor deve ser validado para garantir consistência
- Deve haver um botão salvar para registrar o novo valor

### 4. Como usuário, eu quero alterar o consumo diário de cada mantimento

- Deverá haver um botão em cada mantimento da lista para iniciar a alteração
- O novo valor deve ser informado em uma caixa de texto
- O novo valor deve ser validado para garantir consistência
- Deve haver um botão salvar para registrar o novo valor

### 5. Como usuário, eu quero adicionar e remover mantimentos do estoque

- Deverá haver um botão em cada mantimento da lista para iniciar a adição
- Deverá haver um botão em cada mantimento da lista para iniciar a remoção
- A quantidade deve ser informada em uma caixa de texto
- A quantidade deve ser validada para garantir consistência
- Deve haver um botão salvar para registrar a transação

### 6. Como usuário, eu quero gerar uma lista de compra diária, semanal ou mensal
- Deve haver um botão para solicitar a lista diária
- Deve haver um botão para solicitar a lista semanal
- Deve haver um botão para solicitar a lista mensal
- A lista de compra deve ser gerada em uma página imprimível ou um pdf